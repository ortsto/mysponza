#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm;
layout (location = 2) in vec2 aUv;
layout (location = 3) in mat4 instanceMatrix;
uniform mat4 transform;
uniform mat4 projection;
uniform mat4 view;
//uniform mat4 camera;
out vec2 oUv;
void main(){
   gl_Position = projection*view*transform*vec4(aPos, 1.0);
   //transform = instanceMatix;
   oUv = aUv;
}