#pragma once

#include <sponza/sponza_fwd.hpp>
#include <tygra/WindowViewDelegate.hpp>
#include <tgl/tgl.h>
#include <glm/glm.hpp>
#include "multipleclass.h"
#include <vector>
#include <memory>

class MyView : public tygra::WindowViewDelegate
{
public:

	MyView();

	~MyView();

	void setScene(const sponza::Context * scene);

private:

	void windowViewWillStart(tygra::Window * window) override;

	void windowViewDidReset(tygra::Window * window,
							int width,
							int height) override;

	void windowViewDidStop(tygra::Window * window) override;

	void windowViewRender(tygra::Window * window) override;

	const sponza::Context * scene_;
	//print one by one and set texture orders.

	void CreateQuad();
	void drawQuadDebug();
	void drawScene(int programToUse);
	void prepareShadows();
	void drawShadows();
	void startTimer();
	void endTimer(const char *text);

	//Texture in order for object draw
	const char* textureNames[13] =
	{
		"resource:///vase_diff.png", //0
		"resource:///sponza_ceiling_a_diff.png",
		"resource:///sponza_thorn_diff.png",	
		"resource:///lion.png",
		"resource:///background.png",
		"resource:///sponza_flagpole_diff.png", //5
		"resource:///sponza_bricks_a_diff.png",
		"resource:///sponza_floor_diff.png",
		"resource:///sponza_fabric_blue_diff.png",
		"resource:///sponza_roof_diff.png", 
		"resource:///chain_texture.png", //10
		"resource:///vase_round.png",
		"resource:///sponzared.png",	  //12
	};
	//Indextexture by objects draw in scene in order 1 by one 1 and choose his texture

	int TextureObjects[30] = {
		0,1,2,3,4,
		2,4,7,2,6,
		2,6,8,9,5,
		2,6,6,2,10,
		11,2,2,2,12,
		1,2,-1,-1,-1
	};
	std::vector<unsigned int> textureID;
	
	class GBuffer {
	public :
		GBuffer(){}
		~GBuffer() {}
		unsigned int program_;
		unsigned int gBuffer;
		unsigned int gPosition, gNormal, gAlbedoSpec;
		unsigned int gDepth;
		unsigned int rboDepth;
	};
	struct BasicPointLight
	{
		int id;
		glm::vec3 position;
		float range;
		glm::vec3 color;
	};
	struct BasicDirectionalLight
	{
		int id;
		glm::vec3 direction;
		glm::vec3 color;
	};
	struct BasicSpotLight
	{
		int id;
		glm::vec3 position;
		glm::vec3 direction;
		glm::vec3 color;
		float coneAngleDegrees;
		float range;
		bool castshadow;
	};

	//NOT REALLY IN USE SINCE THE POSITION IS CHANGING STORAGE IS USELESS
	std::vector<BasicPointLight> infoPoint_;
	std::vector<BasicDirectionalLight> infoDirect_;
	std::vector<BasicSpotLight> infoSpot_;

	//Programs
	int quadProgram_;
	int shadowProgram_;

	std::vector<glm::mat4> modelMatrix;
	//Storage Mesh
	std::vector<MultipleClass::MeshInfo> infoMesh_;
	MultipleClass::MeshInfo quadDeffered;
	unsigned int texture;

	struct ShadowInfor {
		unsigned int depthMapFBO;
		unsigned int depthMap;

	};
	ShadowInfor useShadow;
	GBuffer usedDeferred;
	glm::ivec2 screenSize;
	glm::vec3 AmbienLight;
	
	unsigned int FramebufferID;
	unsigned int textureFramebuffer;
	// shadows variables
	unsigned int shadowbufferID;
	unsigned int shadowTexture;

	
	//TIMING GL QUERY
	GLuint mQueries[2];
	GLuint64 mTimeStart, mTimeEnd;
	GLuint mDone = 0;

};
