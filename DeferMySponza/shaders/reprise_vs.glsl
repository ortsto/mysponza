#version 330
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm;
layout (location = 2) in vec2 aUv;

uniform mat4 transform;
uniform mat4 view;
uniform mat4 projection;

out vec3 oPos;
out vec3 oNorm;
out vec2 oUv;


void main(void)
{
	vec4 worldPos = transform*vec4(aPos,1.0);
	oPos = worldPos.xyz;
	oUv = aUv;
	mat3 normalMat = transpose(inverse(mat3(transform)));
	oNorm = normalMat*aNorm;
    gl_Position = projection*view*worldPos;
}
