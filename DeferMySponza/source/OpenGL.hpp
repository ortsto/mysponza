#pragma once

#include <sponza/sponza_fwd.hpp>
#include <vector>
#include <tgl/tgl.h>
#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "multipleclass.h"
namespace OPENGL {

	

	
	void useProgram(int prog);
	void deleteProgram(int prog);
	int createShader(const char* vertex,const char* fragment, const char* geometry);
	void checkCompileErrors(GLuint shader, std::string type);
	void loadGeometry(GLuint *vao, GLuint *evo, GLuint *indices,
		std::vector<sponza::Vector3> pos, std::vector<sponza::Vector3> normal, std::vector<sponza::Vector2> uv, std::vector<unsigned int>index);
	void loadArrayMatrix(std::vector<glm::mat4> matrixArray,std::vector<MultipleClass::MeshInfo> mInfo);
	void draw(GLuint vao, GLuint indices);
	unsigned int createTexture(int ,int ,int ,unsigned char *);
	int createFrameBufferTexture(glm::ivec2 screenSize,int tipo);

	void loadGeometryQuad(GLuint *vao, GLuint *evo, GLuint *indices,
		std::vector<float> pos, std::vector<float> uv, std::vector<unsigned int>index);


	//Framebuffers
	unsigned int createFrameBuffer();
	unsigned int createTextureandRender(glm::vec2 SS);
	unsigned int createRenderBufferObject(glm::vec2 SS);

	unsigned int ShadowCreation(unsigned int);
}