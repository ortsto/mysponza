#version 330

layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 aUv;

out vec2 oUv;

//out vec4 fragment_colour;
void main(void)
{
	oUv = aUv;
    gl_Position = vec4(aPos, 1.0);
}
