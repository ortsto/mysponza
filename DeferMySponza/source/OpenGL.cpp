#include "OpenGL.hpp"

#include <tygra/FileHelper.hpp>
#include <iostream>

namespace OPENGL {

	void OPENGL::useProgram(int prog) {
		glUseProgram(prog);
	}

	void OPENGL::deleteProgram(int prog) {
		glDeleteProgram(prog);
	}
	int OPENGL::createShader(const char * vertex,const char* fragment, const char* geometry = nullptr) {
		int vertexID;
		int fragmentID;
		int geometryID;
		GLint res;
		GLchar log[512];

		const GLchar* vertexSource;
		const GLchar* fragmentSource;
		const GLchar* geometrySource;
		std::string sourceV;
		std::string sourceF;
		std::string sourceG;
		sourceV = tygra::createStringFromFile(vertex);
		//printf("\n%s\n", sourceV.c_str());
		vertexSource = sourceV.c_str();
		vertexID = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexID, 1, &vertexSource, nullptr);
		glCompileShader(vertexID);
		glGetShaderiv(vertexID, GL_COMPILE_STATUS, &res);
		if (!res)
		{
			glGetShaderInfoLog(vertexID, 512, nullptr, log);
			printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED ");
			printf("\n%s", log);
		}


		sourceF = tygra::createStringFromFile(fragment);
		//printf("\n%s\n", sourceF.c_str());
		fragmentSource = sourceF.c_str();
		fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentID, 1, &fragmentSource, nullptr);
		glCompileShader(fragmentID);
		glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &res);
		if (!res)
		{
			glGetShaderInfoLog(fragmentID, 512, nullptr, log);
			printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED ");
			printf("\n%s", log);
		}

		if (geometry != nullptr)
		{
			sourceG = tygra::createStringFromFile(geometry);
			geometrySource = sourceG.c_str();
			geometryID = glCreateShader(GL_GEOMETRY_SHADER);
			glShaderSource(geometryID, 1, &geometrySource, nullptr);
			glCompileShader(geometryID);
			glGetShaderiv(geometryID, GL_COMPILE_STATUS, &res);
			if (!res)
			{
				glGetShaderInfoLog(geometryID, 512, nullptr, log);
				printf("ERROR::SHADER::GEOMETRY::COMPILATION_FAILED ");
				printf("\n%s", log);
			}

		}

		//TODO LINK AND CREATION
		int id = glCreateProgram();
		glAttachShader(id, vertexID);
		glAttachShader(id, fragmentID);
		if(geometry != nullptr) {
			glAttachShader(id, geometryID);
		}

		glLinkProgram(id);
		checkCompileErrors(id, "PROGRAM");
		glGetProgramiv(id, GL_LINK_STATUS, &res);
		if (!res)
		{
			printf("ERROR::SHADER::PROGRAM::LINKING_FAILED ");
			printf("\n ERROR: Compile failed:\n  %s \n  %s \n%s. \n\n", vertex, fragment,geometry);
			glGetProgramInfoLog(id, 512, nullptr, log);
			//printf("Failed to link the shaders:\n");
			printf("%s \n", log);
		}
		else
		{
			printf("Created a program with id:%i!\n", id);
		}
		sourceF.clear();
		sourceV.clear();
		if (geometry != nullptr) {
			sourceG.clear();
			glDeleteShader(geometryID);
		}
		glDeleteShader(vertexID);
		glDeleteShader(fragmentID);
		return id;
	}


	void OPENGL::checkCompileErrors(GLuint shader, std::string type)
	{
		GLint success;
		GLchar infoLog[1024];
		if (type != "PROGRAM")
		{
			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
			if (!success)
			{
				glGetShaderInfoLog(shader, 1024, NULL, infoLog);
				std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
			}
		}
		else
		{
			glGetProgramiv(shader, GL_LINK_STATUS, &success);
			if (!success)
			{
				glGetProgramInfoLog(shader, 1024, NULL, infoLog);
				std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
			}
		}
	}

	void OPENGL::loadGeometry(GLuint *VAO, GLuint *EVO, GLuint *indices,
		std::vector<sponza::Vector3> pos, std::vector<sponza::Vector3> norm, std::vector<sponza::Vector2> uv, std::vector<unsigned int>index) {
		//GLuint posVBO, norVBO, tanVBO, uvVBO;
		std::vector<float> vertex;
		std::vector<float> normal;
		std::vector<float> uvv;
		for (int i = 0; i < pos.size(); i++) {
			vertex.push_back(pos[i].x);
			vertex.push_back(pos[i].y);
			vertex.push_back(pos[i].z);

			normal.push_back(norm[i].x);
			normal.push_back(norm[i].y);
			normal.push_back(norm[i].z);
		}
		for (int i = 0; i < uv.size(); i++) {
			uvv.push_back(uv[i].x);
			uvv.push_back(uv[i].y);
		}

		GLuint posVBO;
		GLuint norVBO;
		GLuint uvVBO;

		glGenVertexArrays(1, VAO);
		glBindVertexArray(*VAO);

		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertex.size(), &(*vertex.begin()), GL_STATIC_DRAW);

		glGenBuffers(1, &norVBO);
		glBindBuffer(GL_ARRAY_BUFFER, norVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*normal.size(), &(*normal.begin()), GL_STATIC_DRAW);

		*indices = index.size();
		if (!uvv.empty()) {
			glGenBuffers(1, &uvVBO);
			glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float)*uvv.size(), &(*uvv.begin()), GL_STATIC_DRAW);
		}
		glGenBuffers(1, EVO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *EVO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*index.size(), &(*index.begin()), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, norVBO);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		if (!uvv.empty()) {
			glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
			glEnableVertexAttribArray(2);
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		}

		//Unbind all
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	void OPENGL::loadArrayMatrix(std::vector<glm::mat4> matrixArray, std::vector<MultipleClass::MeshInfo> mInfo) {
		unsigned int buffer;
		glGenBuffers(1, &buffer);
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		int amount = matrixArray.size();
		glBufferData(GL_ARRAY_BUFFER, amount * sizeof(glm::mat4), &(*matrixArray.begin()), GL_STATIC_DRAW);
		for (unsigned int i = 0; i < mInfo.size(); i++) {
			
			unsigned int vao = mInfo[i].vao;
			printf("\nIterator %d size %d", i , mInfo[i].sizeMeshObjects);
			for (unsigned int numberOfInstances=0; numberOfInstances < mInfo[i].sizeMeshObjects; numberOfInstances++) {
				glBindVertexArray(vao);
				// set attribute pointers for matrix (4 times vec4)
				glEnableVertexAttribArray(3);
				glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
				glEnableVertexAttribArray(4);
				glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
				glEnableVertexAttribArray(5);
				glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
				glEnableVertexAttribArray(6);
				glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

				glVertexAttribDivisor(3, 1);
				glVertexAttribDivisor(4, 1);
				glVertexAttribDivisor(5, 1);
				glVertexAttribDivisor(6, 1);

				glBindVertexArray(0);
			}
		
		}

	}

	void OPENGL::draw(GLuint vao,  GLuint indices) {
		//id vao
		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, 0, GL_UNSIGNED_INT, &indices);
		
	}

	unsigned int OPENGL::createTexture(int width,int height,int channels,unsigned char * data) {

		unsigned int texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture); 							   
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


		if (data)
		{
			if (channels == 3)
			{
				printf("\nRGB");
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
				glGenerateMipmap(GL_TEXTURE_2D);
			}
			else
			{
				printf("\nRGBA");
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
				glGenerateMipmap(GL_TEXTURE_2D);
			}

		}
		else
		{
			printf("- Error loading texture...\n");
		}
	//	stbi_image_free(data);

		return texture;
	}


	int OPENGL::createFrameBufferTexture(glm::ivec2 screenSize ,int tipo) {
		unsigned int framebuffer;
		glGenTextures(1, &framebuffer);
		glBindTexture(GL_TEXTURE_2D, framebuffer);

		if (tipo == 0) {
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, screenSize.x, screenSize.y, 0, GL_RGB, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framebuffer, 0);

		}
		else if (tipo == 1) {
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, screenSize.x, screenSize.y, 0, GL_RGB, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, framebuffer, 0);

		}
		else if (tipo == 2)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, screenSize.x, screenSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, framebuffer, 0);
		}
		else if (tipo == 3) {
		
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, screenSize.x, screenSize.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, framebuffer, 0);
		}
		return framebuffer;
	}

	void OPENGL::loadGeometryQuad(GLuint *VAO, GLuint *EVO, GLuint *indices,
		std::vector<float> vertex, std::vector<float> uvv, std::vector<unsigned int>index) {
		//GLuint posVBO, norVBO, tanVBO, uvVBO;


		GLuint posVBO;
		GLuint norVBO;
		GLuint uvVBO;

		glGenVertexArrays(1, VAO);
		glBindVertexArray(*VAO);

		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertex.size(), &(*vertex.begin()), GL_STATIC_DRAW);


		*indices = index.size();

		glGenBuffers(1, &uvVBO);
		glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*uvv.size(), &(*uvv.begin()), GL_STATIC_DRAW);
		
		glGenBuffers(1, EVO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *EVO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*index.size(), &(*index.begin()), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		
		glBindBuffer(GL_ARRAY_BUFFER, uvVBO);
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		

		//Unbind all
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	unsigned int OPENGL::createFrameBuffer() {
		unsigned int framebuffer;
		glGenFramebuffers(1, &framebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
		return framebuffer;
	}

	unsigned int OPENGL::createTextureandRender(glm::vec2 SS) {
		unsigned int  texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SS.x, SS.y, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			printf("ERROR::FRAMEBUFFER:: Framebuffer is not complete!");

		unsigned int rbo;
		glGenRenderbuffers(1, &rbo);
		glBindRenderbuffer(GL_RENDERBUFFER, rbo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, SS.x, SS.y); // use a single renderbuffer object for both a depth AND stencil buffer.
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo); // now actually attach it
																									  // now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			printf("\nERROR::FRAMEBUFFER:: Framebuffer is not complete!");
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		return texture;
	}



	unsigned int OPENGL::createRenderBufferObject(glm::vec2 SS) {
	
		return 0;
	}
	unsigned int OPENGL::ShadowCreation(unsigned int depthMapFBO) {
		unsigned int depthMap;
		glGenTextures(1, &depthMap);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// attach depth texture as FBO's depth buffer
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		return depthMap;
	}
}