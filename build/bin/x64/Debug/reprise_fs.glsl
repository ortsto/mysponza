#version 330

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

in vec3 oPos;
in vec3 oNorm;
in vec2 oUv;

uniform sampler2D tex;

void main(void)
{
	gPosition = oPos;
	gNormal = normalize(oNorm);
	gAlbedoSpec.rgb = texture(tex, oUv).rgb;
  
}
