#version 330 core
out vec4 FragColor;
  
in vec2 oUv;

uniform sampler2D screenPosition;
uniform sampler2D screenNormal;
uniform sampler2D screenAlbedo;
uniform sampler2D screenDepth;

struct Light {
    vec3 position;
    vec3 color;
    
    float range;
    vec3 direction;
};
struct DLight {
	vec3 color;
	vec3 direction;
};

struct SPLight {
    vec3 color;
    vec3 direction;
    vec3 position;
    float cone_angle;
    float range;
};


const int NR_LIGHTS = 20;
const int NR_DLIGHTS = 2;
const int NR_SPLIGHTS = 5;
uniform Light lights[NR_LIGHTS];
uniform DLight dlights[NR_DLIGHTS];
uniform SPLight splight[NR_SPLIGHTS];
uniform vec3 viewPos;
uniform float AmbienIntesity;

vec3 DirectionalLight(vec3 normal, DLight dlights);

vec3 PointLightCalc(Light nlight);
vec3 DiffuseLightCalc(vec3 lightPos, vec3 lightInt, float attenuation);
vec3 SpotLightCalc(SPLight nlight);
vec3 DiffuseSpotLightCalc(vec3 lightPos, vec3 lightInt, float attenuation);
vec3 position;
vec3 normal;
vec3 diffuse;
void main()
{ 

   

	position = texture(screenPosition, oUv).rgb;
    normal = texture(screenNormal, oUv).rgb;
    diffuse = texture(screenAlbedo, oUv).rgb;
    float SpecularTex = texture(screenAlbedo, oUv).a;
    float depthValue = texture(screenDepth, oUv).r;
	vec3 ambientL = vec3(0.1f, 0.1f, 0.1f)*diffuse;
	vec3 colourAmbient = ambientL;
    normal = normalize(normal);
    vec3 colour;

    //colour += DirectionalLight(normal,directional_light_intensity);
	for (int i = 0; i<NR_DLIGHTS; i++) {
		colourAmbient += DirectionalLight(normal, dlights[i]);
	}
	

    for(int i = 0;i<NR_LIGHTS;i++){
        colour += PointLightCalc(lights[i]);
    }
    vec3 spotlightColour= vec3(0,0,0);
    for(int i = 0;i<NR_SPLIGHTS;i++){
        spotlightColour += SpotLightCalc(splight[i]);
    }

	vec3 result =   colourAmbient+ colour +spotlightColour;
	result = result * (diffuse*0.3f);
    FragColor = vec4(result,1.0f);
}

vec3 DirectionalLight(vec3 normal, DLight dlights)
{
    float scale = max(0, dot(normalize(normal), dlights.direction));

    vec3 intensity = vec3(dlights.color.x, dlights.color.y, dlights.color.z)*scale;

    return intensity;
}

vec3 DiffuseLightCalc(vec3 lightPos, vec3 lightInt, float attenuation)
{
    vec3 L = normalize(lightPos - position);
    float scaler = max(0, dot(L, normal))*attenuation;

    vec3 diffuse_intensity = lightInt * scaler;

    return diffuse_intensity;
}

vec3 PointLightCalc(Light nlight)
{
    float dist = distance(nlight.position, position);
    float attenuation = 1 - smoothstep(1.f, nlight.range, dist);

    vec3 colour = DiffuseLightCalc(nlight.position, nlight.color, attenuation);

    return colour;
}

vec3 SpotLightCalc(SPLight splight)
{
    vec3 colour = vec3(0, 0, 0);
    float direction = dot(normalize(splight.position - position), -splight.direction);
    float effect = smoothstep(cos(splight.cone_angle), cos(splight.cone_angle / 2), direction);

    float distance = distance(splight.position, position);
    float attenuation = 1 - smoothstep(0.0, splight.range, distance);

    vec3 diffuse_intensity = DiffuseLightCalc(splight.position, splight.color, attenuation);

    colour += (diffuse_intensity * effect);

    return colour;
}

vec3 DiffuseSpotLightCalc(vec3 lightPos, vec3 lightInt, float attenuation)
{
    vec3 L = normalize(lightPos - position);
    float scaler = max(0, dot(L, normal))*attenuation;

    vec3 diffuse_intensity = lightInt * scaler * diffuse;

    return diffuse_intensity;
}