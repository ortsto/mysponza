#include "MyView.hpp"

#include <sponza/sponza.hpp>
#include <tygra/FileHelper.hpp>
#include <tsl/shapes.hpp>
#include <glm/glm.hpp> // vec3 normalize reflect dot pow
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <glm/gtc/random.hpp>
#include <iostream>
#include <cassert>
#include "OpenGL.hpp"
#include <math.h>

MyView::MyView()
{
}

MyView::~MyView() {
}

void MyView::setScene(const sponza::Context * scene)
{
	scene_ = scene;
}

void MyView::windowViewWillStart(tygra::Window * window)
{
	assert(scene_ != nullptr);
	glEnable(GL_DEPTH_TEST);
	
	printf("\nFor textures ");
	//UPLOAD TEXTURES
	for (unsigned int i = 0; i < 13; i++) {
		tygra::Image tmp = tygra::createImageFromPngFile(std::string(textureNames[i]).c_str());
		unsigned int textmp = OPENGL::createTexture((int)tmp.width(), (int)tmp.height(),(int) tmp.componentsPerPixel(), (unsigned char*)tmp.pixelData());
		//printf("\n %d %s ", i,textureNames[i]);
		if (textmp != 0) {
			textureID.push_back(textmp);
		}
	}

	//CREATE SHADERS 
	usedDeferred.program_ = OPENGL::createShader("resource:///reprise_vs.glsl", "resource:///reprise_fs.glsl", nullptr);
	shadowProgram_ = OPENGL::createShader("resource:///shadow_mapping_vs.glsl", "resource:///shadow_mapping_fs.glsl", nullptr);
	quadProgram_ = OPENGL::createShader("resource:///simpleFramebuffer_vs.glsl", "resource:///simpleFramebuffer_fs.glsl", nullptr);

	auto geoBuilder = sponza::GeometryBuilder();
	int idValue_ = 0;
	//SAVE BUFFERS AND LOAD GEOMETRY FROM SCENE
	for each(sponza::Mesh mesh in geoBuilder.getAllMeshes()) {
		//printf("\nID %d index %d", mesh.getId(), mesh.getPositionArray().size());
		//printf("\n %d  %d", mesh.getTextureCoordinateArray().size(), mesh.getNormalArray().size());
		MultipleClass::MeshInfo tmp;
		tmp.color_ = glm::vec4(glm::linearRand(0.f, 1.0f), glm::linearRand(0.f, 1.0f), glm::linearRand(0.f, 1.0f),1.0f);
		OPENGL::loadGeometry(&tmp.vao, &tmp.evo, &tmp.indices, mesh.getPositionArray(),mesh.getNormalArray(), mesh.getTextureCoordinateArray(), mesh.getElementArray());
		tmp.id = mesh.getId();
		//printf("\nID MESH %d", tmp.id);
		infoMesh_.push_back(tmp);
	}
	//printf("\nMeshes %zu Textures %zu\n", infoMesh_.size(), textureID.size());
	//Create ARRAY MATRIX
	int contador = 0;
	for (int i = 0; i < infoMesh_.size(); i++) {
		auto meshInstances = scene_->getInstancesByMeshId(infoMesh_[i].id);
		for (unsigned int j = 0; j < meshInstances.size(); j++)

		{
			infoMesh_[i].sizeMeshObjects = (int)meshInstances.size();
			glm::mat4 model;
			auto tMatrix = scene_->getInstanceById(meshInstances[j]).getTransformationMatrix();

			glm::mat4x3 model_4x3;
			model_4x3 = glm::make_mat4x3(&tMatrix.m00);
			model = glm::mat4(model_4x3);
			modelMatrix.push_back(model);
			contador++;
		}
	}

	//printf("\nMeshInfo %d ModelMatrix %d\n", contador, (int)modelMatrix.size());
	////////// DEFERRRED PART ///////////


	CreateQuad();
	//
	GLint viewport_size[4];
	glGetIntegerv(GL_VIEWPORT, viewport_size);
	printf("\nSize %d %d %d %d", viewport_size[0], viewport_size[1], viewport_size[2], viewport_size[3]);
	screenSize.x = viewport_size[2];
	screenSize.y = viewport_size[3];


	//Deferred
	

	glGenFramebuffers(1, &usedDeferred.gBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, usedDeferred.gBuffer);

	usedDeferred.gPosition = OPENGL::createFrameBufferTexture(screenSize,0);
	usedDeferred.gNormal = OPENGL::createFrameBufferTexture(screenSize,1);
	usedDeferred.gAlbedoSpec = OPENGL::createFrameBufferTexture(screenSize,2);
	//usedDeferred.gDepth = OPENGL::createFrameBufferTexture(screenSize, 3);
	unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, attachments);
	glGenRenderbuffers(1, &usedDeferred.rboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, usedDeferred.rboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, screenSize.x, screenSize.y);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, usedDeferred.rboDepth);
	// finally check if framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	
		
	///////// SHADOWS


	glGenFramebuffers(1, &useShadow.depthMapFBO);
	useShadow.depthMap = OPENGL::ShadowCreation(useShadow.depthMapFBO);

	//for each (sponza::PointLight var in scene_->getAllPointLights())
	//{
	//	BasicPointLight tmp;
	//	tmp.id = var.getId();
	//	tmp.position.x = var.getPosition().x;
	//	tmp.position.y = var.getPosition().y;
	//	tmp.position.z = var.getPosition().z;
	//	tmp.range = var.getRange();
	//	tmp.color.x = var.getIntensity().x;
	//	tmp.color.y = var.getIntensity().y;
	//	tmp.color.z = var.getIntensity().z;
	//	//printf("\nColor %f %f %f\n", tmp.color.x, tmp.color.y, tmp.color.z);
	//	infoPoint_.push_back(tmp);
	//}
	//for each (sponza::DirectionalLight var in scene_->getAllDirectionalLights())
	//{
	//	BasicDirectionalLight tmp;
	//	tmp.id = var.getId();
	//	tmp.direction.x = var.getDirection().x;
	//	tmp.direction.y = var.getDirection().y;
	//	tmp.direction.z = var.getDirection().z;
	//	tmp.color.x = var.getIntensity().x;
	//	tmp.color.y = var.getIntensity().y;
	//	tmp.color.z = var.getIntensity().z;
	//	infoDirect_.push_back(tmp);
	//}
	//for each (sponza::SpotLight var in scene_->getAllSpotLights())
	//{
	//	BasicSpotLight tmp;
	//	tmp.id = var.getId();
	//	tmp.position.x = var.getDirection().x;
	//	tmp.position.y = var.getDirection().y;
	//	tmp.position.z = var.getDirection().z;
	//	tmp.direction.x = var.getDirection().x;
	//	tmp.direction.y = var.getDirection().y;
	//	tmp.direction.z = var.getDirection().z;
	//	tmp.color.x = var.getIntensity().x;
	//	tmp.color.y = var.getIntensity().y;
	//	tmp.color.z = var.getIntensity().z;
	//	tmp.coneAngleDegrees = var.getConeAngleDegrees();
	//	tmp.range = var.getRange();
	//	tmp.castshadow = var.getCastShadow();
	//	infoSpot_.push_back(tmp);
	//}
	AmbienLight.x = scene_->getAmbientLightIntensity().x;
	AmbienLight.y = scene_->getAmbientLightIntensity().y;
	AmbienLight.z = scene_->getAmbientLightIntensity().z;
	
	//printf("\nInfo light P %d D %d  SP %d\n", infoPoint_.size(),infoDirect_.size(),infoSpot_.size());




	//TIMING
	glGenQueries(2, mQueries);
	


}

void MyView::windowViewDidReset(tygra::Window * window,
								int width,
								int height)
{
	glViewport(0, 0, width, height);

}

void MyView::windowViewDidStop(tygra::Window * window)
{
	//DELETE MESH INFO AND QUAD INFO
	for (int i = 0; i < infoMesh_.size(); i++) {
		glDeleteBuffers(1, &infoMesh_[i].vao);
		glDeleteBuffers(1, &infoMesh_[i].evo);
		glDeleteBuffers(1, &infoMesh_[i].indices);
	}

	glDeleteBuffers(1, &quadDeffered.vao);
	glDeleteBuffers(1, &quadDeffered.evo);
	glDeleteBuffers(1, &quadDeffered.indices);

	//Delete gbuffer and textures
	glDeleteFramebuffers(1, &usedDeferred.gBuffer);
	glDeleteTextures(1, &usedDeferred.gPosition);
	glDeleteTextures(1, &usedDeferred.gNormal);
	glDeleteTextures(1, &usedDeferred.gAlbedoSpec);
	glDeleteProgram(usedDeferred.program_);
	glDeleteProgram(quadProgram_);
	glDeleteProgram(shadowProgram_);
}

void MyView::windowViewRender(tygra::Window * window)
{
	assert(scene_ != nullptr);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	startTimer();
	glEnable(GL_DEPTH_TEST);

	//SHADOW PART
	glViewport(0, 0, 1024, 1024);
	glBindFramebuffer(GL_FRAMEBUFFER, useShadow.depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	drawShadows();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	endTimer("ShadowMaps");

	// GBUFFER PART
	glViewport(0, 0, screenSize.x, screenSize.y);
	startTimer();
	glBindFramebuffer(GL_FRAMEBUFFER, usedDeferred.gBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	drawScene(usedDeferred.program_);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	endTimer("GBuffer");
	

	//Draw quad and compute lightcalc
	startTimer();
	drawQuadDebug();
	endTimer("drawQuad+ lightcalc");

	//COPY FRAMEBUFFER
	glBindFramebuffer(GL_READ_FRAMEBUFFER, usedDeferred.gBuffer);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); 
	glBlitFramebuffer(0, 0, screenSize.x, screenSize.y, 0, 0, screenSize.x, screenSize.y, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
}


void MyView::drawScene(int programToUse) {
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLint viewport_size[4];
	glGetIntegerv(GL_VIEWPORT, viewport_size);
	//printf("Size %d %d %d %d", viewport_size[0], viewport_size[1],viewport_size[2],viewport_size[3]);
	const float aspect_ratio = viewport_size[2] / (float)viewport_size[3];

	//MAKE FUNCTION TO CONVERT SPONZA VECTORS TO GLM VECTORS
	const glm::vec3 camera_position = (const glm::vec3&)scene_->getCamera().getPosition();
	const glm::vec3 camera_direction = (const glm::vec3&)scene_->getCamera().getDirection();
	glm::mat4 projection_xform = glm::perspective(45.f,aspect_ratio,1.0f, 300.f);
	glm::mat4 view_xform = glm::lookAt(camera_position,camera_position + camera_direction,glm::vec3(0, 1, 0));

	for (int i = 0; i < infoMesh_.size(); i++) {
	//for (int i = 26; i < 27; i++) {
		auto meshInstances = scene_->getInstancesByMeshId(infoMesh_[i].id);
		for (unsigned int j = 0; j < meshInstances.size(); j++)

		{
			
			glUseProgram(programToUse);
			
			glm::mat4 model;
			auto tMatrix = scene_->getInstanceById(meshInstances[j]).getTransformationMatrix();			
			glm::mat4x3 model_4x3;
			model_4x3 = glm::make_mat4x3(&tMatrix.m00);
			model = glm::mat4(model_4x3);

			unsigned int tranformLoc = glGetUniformLocation(programToUse, "transform");
			glUniformMatrix4fv(tranformLoc, 1, GL_FALSE, glm::value_ptr(model));
			unsigned int proLoc = glGetUniformLocation(programToUse, "projection");
			glUniformMatrix4fv(proLoc, 1, GL_FALSE, glm::value_ptr(projection_xform));
			unsigned int viewLoc = glGetUniformLocation(programToUse, "view");
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view_xform));

			glBindVertexArray(infoMesh_[i].vao);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, infoMesh_[i].evo);


			int texId = TextureObjects[i];
			
			//For debug the texture in use when draw one by one
			//printf("\n%s\n", textureNames[texId]);
			int useDiff = texId != -1;
			if (useDiff)
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, textureID[texId]);

			}
			glDrawElements(GL_TRIANGLES, infoMesh_[i].indices, GL_UNSIGNED_INT, (void*)0);


			glBindVertexArray(0);

			glUseProgram(0);

		}

	}
}


void MyView::CreateQuad() {
	float vertexData[12]{
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, 1.0, 1.0,
		-1.0, 1.0, 1.0
	};

	float uvData[8]{
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	};
	unsigned int indices[6] = {
		0, 1, 2, 0, 2, 3, // front
	};

	std::vector<float> vertex;
	std::vector<float> uv;
	std::vector<unsigned int> index;

	for (int i = 0; i < 12; i++)
	{
		vertex.push_back(vertexData[i]);
	}
	for (int i = 0; i < 8; i++)
	{
		uv.push_back(uvData[i]);
	}
	for (int i = 0; i < 6; i++)
	{
		index.push_back(indices[i]);
	}
	//LOAD QUAD
	OPENGL::loadGeometryQuad(&quadDeffered.vao, &quadDeffered.evo, &quadDeffered.indices, vertex , uv, index);
}

void MyView::prepareShadows() {
	shadowbufferID = OPENGL::createFrameBuffer();
	shadowTexture;
}



void MyView::drawShadows() {

	glm::vec3 lightPos = (const glm::vec3&)scene_->getAllSpotLights()[0].getPosition();
	for (int i = 0; i < infoMesh_.size(); i++) {
		auto meshInstances = scene_->getInstancesByMeshId(infoMesh_[i].id);
		for (unsigned int j = 0; j < meshInstances.size(); j++)

		{

			glUseProgram(shadowProgram_);

			glm::mat4 model;
			auto tMatrix = scene_->getInstanceById(meshInstances[j]).getTransformationMatrix();
			glm::mat4x3 model_4x3;
			model_4x3 = glm::make_mat4x3(&tMatrix.m00);
			model = glm::mat4(model_4x3);
			unsigned int tranformLoc = glGetUniformLocation(shadowProgram_, "transform");
			glUniformMatrix4fv(tranformLoc, 1, GL_FALSE, glm::value_ptr(model));

			glm::mat4 lightProjection, lightView;
			glm::mat4 lightSpaceMatrix;
			float near_plane = 0.1f, far_plane = 100.f;
			lightProjection = glm::ortho(-20.0f, 20.0f, -20.0f, 20.0f, -10.0f, 100.0f);

			lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
			lightSpaceMatrix = lightProjection * lightView;

			unsigned int proLoc = glGetUniformLocation(shadowProgram_, "lightSpaceMatrix");
			glUniformMatrix4fv(proLoc, 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

			glBindVertexArray(infoMesh_[i].vao);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, infoMesh_[i].evo);


			int texId = TextureObjects[i];
			int useDiff = texId != -1;
			if (useDiff)
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, textureID[texId]);

			}



			//glDrawArrays(GL_TRIANGLES, 0, infoMesh_[i].indices);
			glDrawElements(GL_TRIANGLES, infoMesh_[i].indices, GL_UNSIGNED_INT, (void*)0);

			glBindVertexArray(0);

			glUseProgram(0);

		}

	}
}
void MyView::drawQuadDebug() {
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glDisable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT);

	OPENGL::useProgram(quadProgram_);
	glBindVertexArray(quadDeffered.vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadDeffered.evo);

	glUniform1i(glGetUniformLocation(quadProgram_, "screenPosition"), 0);
	glUniform1i(glGetUniformLocation(quadProgram_, "screenNormal"), 1);
	glUniform1i(glGetUniformLocation(quadProgram_, "screenAlbedo"), 2);
	glUniform1i(glGetUniformLocation(quadProgram_, "screenDepth"), 3);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, usedDeferred.gPosition);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, usedDeferred.gNormal);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, usedDeferred.gAlbedoSpec);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, useShadow.depthMap);



	glUniform1f(glGetUniformLocation(quadProgram_, "AmbienIntesity"),AmbienLight.x );
	for (unsigned int i = 0; i < scene_->getAllPointLights().size(); i++)
	{
		glm::vec3 position = (const glm::vec3&)scene_->getAllPointLights()[i].getPosition();
		glm::vec3 color = (const glm::vec3&)scene_->getAllPointLights()[i].getIntensity();
		float range = scene_->getAllPointLights()[i].getRange();
		std::string tmpPosition = "lights[" + std::to_string(i) + "].position";
		std::string tmpColor = "lights[" + std::to_string(i) + "].color";
		glUniform3fv(glGetUniformLocation(quadProgram_, tmpPosition.c_str()), 1,glm::value_ptr(position));
		glUniform3fv(glGetUniformLocation(quadProgram_, tmpColor.c_str()), 1, glm::value_ptr(color));
		std::string tmpRange = "lights[" + std::to_string(i) + "].range";
		glUniform1f(glGetUniformLocation(quadProgram_, tmpRange.c_str()), range);
	}


	const auto& spotlights = scene_->getAllSpotLights();
	for (unsigned int i = 0; i < scene_->getAllSpotLights().size(); i++)
	{
		glm::vec3 spotlight_intensity = (const glm::vec3&)spotlights[i].getIntensity();
		glm::vec3 spotlight_direction = (const glm::vec3&)spotlights[i].getDirection();
		glm::vec3 spotlight_position = (const glm::vec3&)spotlights[i].getPosition();
		float cone_angle = (const float&)spotlights[i].getConeAngleDegrees();
		float range = (const float&)spotlights[i].getRange();
		bool spotlight_castshadow = (const bool&)spotlights[i].getCastShadow();


		std::string tmpColor = "splight[" + std::to_string(i) + "].color";
		std::string tmpDirection = "splight[" + std::to_string(i) + "].direction";
		std::string tmpPosition = "splight[" + std::to_string(i) + "].position";
		std::string tmpCone = "splight[" + std::to_string(i) + "].cone_angle";
		std::string tmpRange = "splight[" + std::to_string(i) + "].range";
		
		glUniform3fv(glGetUniformLocation(quadProgram_, tmpColor.c_str()), 1, glm::value_ptr(spotlight_intensity));
		glUniform3fv(glGetUniformLocation(quadProgram_, tmpDirection.c_str()), 1, glm::value_ptr(spotlight_direction));
		glUniform3fv(glGetUniformLocation(quadProgram_, tmpPosition.c_str()), 1, glm::value_ptr(spotlight_position));

		glUniform1f(glGetUniformLocation(quadProgram_, tmpCone.c_str()), cone_angle);
		glUniform1f(glGetUniformLocation(quadProgram_, tmpRange.c_str()), range);

	}
	const auto& dirlight = scene_->getAllDirectionalLights();
	for (unsigned int i = 0; i < scene_->getAllDirectionalLights().size(); i++)
	{
		glm::vec3 dirlight_intensity = (const glm::vec3&)spotlights[i].getIntensity();
		glm::vec3 dirlight_direction = (const glm::vec3&)spotlights[i].getDirection();
		std::string tmpColor = "dlights[" + std::to_string(i) + "].color";
		std::string tmpDirection = "dlights[" + std::to_string(i) + "].direction";

		glUniform3fv(glGetUniformLocation(quadProgram_, tmpColor.c_str()), 1, glm::value_ptr(dirlight_intensity));
		glUniform3fv(glGetUniformLocation(quadProgram_, tmpDirection.c_str()), 1, glm::value_ptr(dirlight_direction));


	}

	glm::vec3 tmpCamera= (const glm::vec3&)scene_->getCamera().getPosition();
	glUniform3fv(glGetUniformLocation(quadProgram_, "viewPos"), 1, glm::value_ptr(tmpCamera));

	glDrawElements(GL_TRIANGLES, quadDeffered.indices, GL_UNSIGNED_INT, (void*)0);


	
	glBindVertexArray(0);
	OPENGL::useProgram(0);
}





void MyView::startTimer() {

	glQueryCounter(mQueries[0], GL_TIMESTAMP);

}

void MyView::endTimer(const char * text) {


	glQueryCounter(mQueries[1], GL_TIMESTAMP);
	if (!mDone) {
		glGetQueryObjectuiv(mQueries[1], GL_QUERY_RESULT_AVAILABLE, &mDone);
	}

	glGetQueryObjectui64v(mQueries[0], GL_QUERY_RESULT, &mTimeStart);
	glGetQueryObjectui64v(mQueries[1], GL_QUERY_RESULT, &mTimeEnd);
	float e = ((float)mTimeEnd - (float)mTimeStart) / 1000000.0f;
	printf("\nTime Elapsed %s: %f ms\n",text, (mTimeEnd - mTimeStart) / 1000000.0);


}