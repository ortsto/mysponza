#version 330

uniform sampler2D screenPosition;
uniform sampler2D screenNormal;
uniform sampler2D screenAlbedo;

uniform vec3 directional_light_direction;
uniform vec3 directional_light_intensity;

out vec4 FragColor;
in vec2 oUv;

vec3 DirectionalLight(vec3 normal, vec3 direction);

void main(void)
{
	//vec2 fragCoord = vec2(gl_FragCoord.xy);

	vec3 position = texture(screenPosition, oUv).xyz;
	vec3 normal = texture(screenNormal, oUv).xyz;
	vec3 material = texture(screenAlbedo, oUv).xyz;

	vec3 ambient_light = vec3(0.1f, 0.1f, 0.1f)*material;

	normal = normalize(normal);

	vec3 colour = ambient_light;

	colour += DirectionalLight(normal, directional_light_direction);

	FragColor = vec4(colour, 1);
}

vec3 DirectionalLight(vec3 normal, vec3 direction)
{
	float scale = max(0, dot(normalize(normal), direction));

	vec3 intensity = vec3(directional_light_intensity.x, directional_light_intensity.y, directional_light_intensity.z)*scale;

	return intensity;
}