#pragma once

#include <vector>
#include <tgl/tgl.h>
#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp>
namespace MultipleClass
{
	class MeshInfo
	{
	public:
		MeshInfo() {}
		~MeshInfo() {}
		GLuint vao, evo, indices;
		glm::vec4 color_;
		int id;
		int sizeMeshObjects;
	private:

	};

}